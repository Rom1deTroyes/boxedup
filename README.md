## BoxedUp

### Summary

BoxedUp is a collection of Ansible roles which are made to bootstrap a modern developer workstation.

This is an alpha release.

---

### Can I submit a PR?

Yes. I'm not going to maintain this by myself if it grows to any reasonable size. Please try to follow commitizen standards.

### Why Ubuntu, etc?

I need something to start with. There will be conditional branches for other things (Debian, etc) later, but support for non-apt systems will have to be entirely community driven, as I simply do not care.

---

### Testing and Trying

The repository comes with a `Dockerfile` which illustrates the basic steps involved in getting Ansible to run on your workstation, in order to execute the playbook. Alternatively, you can run `test-shell.sh` which will use Docker to build a container image, and drop you into a shell on a temporary container to try it out.

From there, you run:

```
ansible-playbook --list-tags boxedup.yml
```

This will list the currently available tags for you. If, for example, you want to bootstrap a basic developer workstation that includes support for C/C++ and Python, you might type:

```
ansible-playbook --tags cpp,python boxedup.yml
```

To install everything, or perform an e2e test, you can use the `everything` tag:

```
ansible-playbook --tags everything boxedup.yml
```
