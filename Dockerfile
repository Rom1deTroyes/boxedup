FROM ubuntu:20.04
ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update
RUN apt-get dist-upgrade -y
RUN apt-get install -y software-properties-common
RUN apt-get install -y ansible
WORKDIR /ansible


